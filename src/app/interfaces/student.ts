export interface Student {
    id:string,
    name:string,
    mathematics:number,
    psychometric:number,
    payment:boolean,
    result?:string,
    email?:string    
    timestamp?:number;
}
