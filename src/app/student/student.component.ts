import { PredictionService } from './../prediction.service';
import { StudentsService } from './../students.service';
import { Student } from './../interfaces/student';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  students:Student[];
  students$;
  editstate = [];
  addStudentFormOpen = false;
  panelOpenState = false;
  rowToSave:number = -1; 
  
 

  deleteStudent(id:string){
    this.studentsService.deleteStudent(id);
  }

  predict(i){
    this.rowToSave = i; 
    console.log(this.students[i]);
    this.predictionService.predict(this.students[i].mathematics, this.students[i].psychometric, this.students[i].payment).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Does not fall off';
        } else {
          var result = 'Falls out'
        }
        this.students[i].result = result;
        console.log(result);
      
      }
    );   
      }

    cancel(i){
      this.rowToSave = null; 
      this.students[i].result = null;
    }

  constructor(public authService:AuthService, private studentsService:StudentsService, private predictionService: PredictionService) { }

  ngOnInit(): void {
        this.students$ = this.studentsService.getStudent();
        this.students$.subscribe(
          docs =>{
            this.students = [];
            for(let document of docs){
              const student:Student = document.payload.doc.data();
              student.id = document.payload.doc.id;
              this.students.push(student);
            }
          }
        )
      }
}
